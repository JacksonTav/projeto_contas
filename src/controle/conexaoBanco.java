package controle;
import java.sql.*;
import javax.swing.JOptionPane;

public class conexaoBanco {
	
	//Respons�vel por preparar e realizar pesquisas no BD 
	public Statement stm; 
	//Respons�vel por armazenar o resultado de uma pesquisa passada para o Statement
	public ResultSet rs;
	//driver de conexao, responsavel por identificar o banco que esta trabalhando e preparar conexao
	private String driver = "org.mysql.Driver"; //string padr�o
	//caminho onde ser� realizado a conex�o com o BD
	//private String caminho = "jdbc:postgresql://localhost:5432/sistemavideoaula"; //string padr�o
	private String caminho = "jdbc:mysql://localhost:3306/projetocontas"; //string padr�o
	//ususario padr�o do postgres
	private String usuario = "root";
	//senha que foi cadastrada ao instalar o banco de dados
	private String senha = "";
	//Respons�vel por realizar a conex�o com o BD de fato
	public Connection conn;

	//Metodos respons�vel por realizar a conex�o com o BD
	public void conexao(){
		try {//tentativa inicial
			//Parametros, jdbc.drivers � o drive de conex�o do netbeans, driver = � o driver da conexao nosso9
	        System.setProperty("jdbc.Drivers", driver); //seta a propriedade do driver de conex�o
	        conn = DriverManager.getConnection(caminho, usuario, senha); //realiza conex�o com o BD
	        // JOptionPane.showMessageDialog(null, "Conectado com sucesso!");  //imprime uma caixa de msg informando situa��o
	    } catch (SQLException ex) { //excess�o
	        JOptionPane.showMessageDialog(null, "Erro de conex�o!\n Erro: " + ex.getMessage());
	    }
	}//Fim do metodo conex�o
	
	//M�todo para simplifcar codigos ao passar codigos sql 
	public void executaSQL(String sql){
		try {
			stm = conn.createStatement(rs.TYPE_SCROLL_INSENSITIVE, rs.CONCUR_READ_ONLY); //difere maiusculo e minusculo
			rs = stm.executeQuery(sql);
	    } catch (SQLException ex) {
	           //JOptionPane.showMessageDialog(null, "Erro de ExecutaSQL!\n Erro: " + ex.getMessage());
	      }
	}
	
	//M�todo para desconectar o banco de dados
	public void desconecta(){
		try {
			conn.close(); //fecha a conex�o
			//JOptionPane.showMessageDialog(null, "Desconectado com sucesso!");
	    } catch (SQLException ex) {
	    	//JOptionPane.showMessageDialog(null, "Erro ao fechar conex�o!\n Erro: " + ex.getMessage());
	                    
	      }
	} //Fim do metodo de desconecta
}
