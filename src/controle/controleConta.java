package controle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelo.modeloBanco;

public class controleConta {
	conexaoBanco conexao = new conexaoBanco();
	conexaoBanco conPesquisa = new conexaoBanco();


	//m�todo para listar todas as contas
	public void getContas() {
		conexao.conexao();
		try {
			conexao.executaSQL("select * from conta order by id");
			PreparedStatement pst = conexao.conn.prepareStatement("select * from conta order by id");
			pst.execute();
		} catch(SQLException ex) {
			JOptionPane.showMessageDialog(null, "Erro ao todas as contas\nErro: " + ex);
		}
	}
	
	public void listarContas() {
		//buscando todas as contas
		 try{
			 PreparedStatement pst = conexao.conn.prepareStatement("select * from conta");
			 ResultSet rs = pst.executeQuery();
			 
			 while(rs.next()) {
				 int id = rs.getInt("id");
				 int agencia = rs.getInt("agencia");
				 int numero_conta = rs.getInt("numero_conta");
				 int codigo_banco = rs.getInt("codigo_banco");
				 System.out.println("Id: " + id);
				 System.out.println("Agencia: " + agencia);
				 System.out.println("Numero da Conta: " + numero_conta);
				 System.out.println("Codigo do Banco: " + codigo_banco);				 
			 }
		 } catch (SQLException ex) {
			 JOptionPane.showMessageDialog(null, "Erro ao listar todas as contas\nErro: " + ex);
		 }
	}
	
	
	public void listarContas2() {
		//buscando todas as contas
		 try{
			 conexao.executaSQL("select * from conta order by id");
			 conexao.rs.first();
			 while(conexao.rs.next()) {
				 int id = conexao.rs.getInt("id");
				 int agencia = conexao.rs.getInt("agencia");
				 int numero_conta = conexao.rs.getInt("numero_conta");
				 int codigo_banco = conexao.rs.getInt("codigo_banco");
				 System.out.println("Id: " + id);
				 System.out.println("Agencia: " + agencia);
				 System.out.println("Numero da Conta: " + numero_conta);
				 System.out.println("Codigo do Banco: " + codigo_banco);				 
			 }
		 } catch (SQLException ex) {
			 JOptionPane.showMessageDialog(null, "Erro ao listar todas as contas\nErro: " + ex);
		 }
	}
	
	
}
