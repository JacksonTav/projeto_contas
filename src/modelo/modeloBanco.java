package modelo;

public class modeloBanco {
	private int id;
	private int agencia;
	private int numero_conta;
	private int codigo_banco;
	
	public int getId() {
		return this.id;
	}
	
	public int getAgencia() {
		return this.agencia;
	}
	
	public int getNumeroConta(){
		return this.numero_conta;
	}
	
	public int getCodigoBanco(){
		return this.codigo_banco;
	}
	
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	
	public void setNumeroConta(int numero) {
		this.numero_conta = numero;
	}
	
	public void setCodBanco(int codigo) {
		this.codigo_banco = codigo;
	}
}
