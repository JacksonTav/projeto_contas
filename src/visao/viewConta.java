package visao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import controle.conexaoBanco;
import controle.controleConta;

public class viewConta {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		conexaoBanco conecta = new conexaoBanco(); //Vari�vel global conexao
		conecta.conexao(); //chamando o m�todo conexao para iniciar a conexao na aplica��o
		
		controleConta ctlConta = new controleConta();
		//buscando todas as contas
		 try{
			 PreparedStatement pst = conecta.conn.prepareStatement("select * from conta");
			 ResultSet rs = pst.executeQuery();
			 
			 while(rs.next()) {
				 int id = rs.getInt("id");
				 int agencia = rs.getInt("agencia");
				 int numero_conta = rs.getInt("numero_conta");
				 int codigo_banco = rs.getInt("codigo_banco");
				 System.out.println("Id: " + id);
				 System.out.println("Agencia: " + agencia);
				 System.out.println("Numero da Conta: " + numero_conta);
				 System.out.println("Codigo do Banco: " + codigo_banco);				 
			 }
		 } catch (SQLException ex) {
			 JOptionPane.showMessageDialog(null, "Erro ao listar todas as contas\nErro: " + ex);
		 }
		 
		 //fechar conex�o com o banco
		 conecta.desconecta();
	}

}
